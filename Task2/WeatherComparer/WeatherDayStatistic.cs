﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherComparer
{
    public class WeatherDayStatistic
    {
        public string Date { get; set; }
        public int Pressure { get; set; } = 0;
        public float Temperature { get; set; } = 0;
        public float TemperatureFeelsLike { get; set; } = 0;
        public float TemperatureMin { get; set; } = 0;
        public float TemperatureMax { get; set; } = 0;
        public float WindSpeed { get; set; } = 0;
        public int Clouds { get; set; } = 0;
        public int Rain { get; set; } = 0;
    }
}
