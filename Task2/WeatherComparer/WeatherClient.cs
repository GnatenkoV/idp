﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Linq;

namespace WeatherComparer
{
    public class WeatherClient : IWeatherClient
    {
        private const string m_openWeatherApiKey = "ed3fb09bdeff6c637b165ec16bd56f00";
        private const string m_positionApiKey = "d6c8319849a1c7fd47ba93e4b616f73b";
        private readonly long m_dayUnixTime = 86400;
        private HttpClient m_httpClient;
        private long getCurrentTime() => (DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
        public static DateTime getDateTimeFromStamp(long unixTimeStamp)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTime = dateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dateTime;
        }
        private Uri generateOpenWeatherRequestUrl(PointF coords, long timeStamp) => new Uri($"https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=" + coords.X + "&lon=" + coords.Y + "&dt=" + timeStamp + "&units=metric&appid=" + m_openWeatherApiKey);
        private Uri generatePositionRequestUrl(string address) => new Uri($"http://api.positionstack.com/v1/forward?access_key={m_positionApiKey}&query={address}");

        public WeatherClient() : base()
        {
            m_httpClient = new HttpClient();
        }
        ~WeatherClient()
        {
            m_httpClient.Dispose();
        }

        async public Task<PointF> GetCityInfoAsync(string country) 
        {
            if (country.Length == 0)
            {
                throw new ArgumentException("Wrong country name: ", country);
            }
            country = country.ToUpper();
            var jsonResponse = await m_httpClient.GetStringAsync(generatePositionRequestUrl(country));
            var jsonData = JObject.Parse(jsonResponse);

            if (!jsonData.HasValues)
            {
                throw new ArgumentException("Wrong country name: ", country);
            }

            float lap = 0, lon = 0;
            bool found = false;
            try
            {
                var citiesList = jsonData.SelectToken("data");
                foreach (var cityData in citiesList)
                {
                    if (!float.TryParse(cityData.SelectToken("latitude").ToString(), out lap) ||
                     !float.TryParse(cityData.SelectToken("longitude").ToString(), out lon))
                    {
                        throw new ArgumentException("Wrong country name: ", country);
                    }

                    if (country.Contains(cityData.SelectToken("name").ToString().ToUpper()))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    throw new ArgumentException("Country's lap and lon not found: ", country);
                }
            }
            catch (NullReferenceException)
            {
                throw new ArgumentException("Wrong country name: ", country);
            }

            return new PointF(lap, lon);
        }

        async public Task<List<WeatherDayStatistic>> GetWeatherInfoAsync(string city, int dayCount)
        {
            if (dayCount < 1 || dayCount > 5)
            {
                throw new ArgumentException($"Wrong days count: {dayCount}");
            }

            var coords = await GetCityInfoAsync(city);

            var result = new List<WeatherDayStatistic>();
            int tempFixCount = 0;
            long time = getCurrentTime();
            for (int i = 1; i <= dayCount; i++)
            {
                try
                {
                    Thread.Sleep(800);
                    var jsonResponse = await m_httpClient.GetStringAsync(generateOpenWeatherRequestUrl(coords, time - m_dayUnixTime*i));
                    result.Add(ParseJson(jsonResponse));
                }
                catch (HttpRequestException ex)
                {
                    if (ex.Message.EndsWith("400 (Bad Request)."))
                    {
                        i--;
                        tempFixCount++;
                    }
                    if (tempFixCount > 10)
                    {
                        throw new ArgumentException("Error, While trying to response data from OpenWeatherApi", ex);
                    }
                }
            }
            return result;
        }
        public WeatherDayStatistic ParseJson(string jsonResponse)
        {
            if (string.IsNullOrEmpty(jsonResponse))
            {
                throw new ArgumentException("Empty Response data");
            }

            var result = new WeatherDayStatistic();
            int hoursRecorded = 0;
            result.TemperatureMin = 100;
            result.TemperatureMax = -100;

            var response = JsonConvert.DeserializeObject<Response.WeatherResponse>(jsonResponse);

            foreach (var weatherHourData in response.hourly)
            {
                try
                {
                    const string cloudsString = "Clouds", rainString = "Rain";
                    hoursRecorded++;
                    result.Date = getDateTimeFromStamp(weatherHourData.dt).ToString("dd-MM-yyyy-HH-mm");

                    string weatherState = weatherHourData.weather.First().main;
                    if (weatherState == cloudsString)
                    {
                        result.Clouds += 1;
                    }
                    else if (weatherState == rainString)
                    {
                        result.Rain += 1;
                    }
                    
                    result.Pressure += weatherHourData.pressure;
                    result.Temperature += weatherHourData.temp;
                    result.TemperatureFeelsLike += weatherHourData.feels_like;
                    
                    if (weatherHourData.temp > result.TemperatureMax)
                    {
                        result.TemperatureMax = weatherHourData.temp;
                    }
                    if (weatherHourData.temp < result.TemperatureMin)
                    {
                        result.TemperatureMin = weatherHourData.temp;
                    }
                    result.WindSpeed += weatherHourData.wind_speed;
                }
                catch (NullReferenceException )
                {
                    throw new ArgumentException("Cannot parse received json data");
                }
            }
            
            result.Temperature = result.Temperature / hoursRecorded;
            result.WindSpeed = result.WindSpeed / hoursRecorded;
            result.Temperature = result.Temperature / hoursRecorded;
            result.TemperatureFeelsLike = result.TemperatureFeelsLike / hoursRecorded;
            result.Pressure = result.Pressure / hoursRecorded;

            return result;
        }
    }
}
