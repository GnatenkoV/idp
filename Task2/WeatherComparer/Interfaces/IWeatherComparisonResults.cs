﻿
using System.Collections.Generic;

namespace WeatherComparer {
    public enum City
    {
        FIRST,
        SECOND
    }
}

namespace WeatherComparer.Interfaces
{
    public interface IWeatherComparisonResults
    {
        public List<WeatherDayStatistic> GetResult(City city);

        public string GetCityName(City city);
    }
}
