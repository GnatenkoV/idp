﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherComparer
{
    public interface IWeatherManager
    {
        public WeatherComparisonResults GetResults(string city1, string city2, int days);
    }
}
