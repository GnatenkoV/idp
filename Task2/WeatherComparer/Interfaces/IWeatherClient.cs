﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;

namespace WeatherComparer 
{
    public interface IWeatherClient
    {
        public Task<PointF> GetCityInfoAsync(string country);
        public Task<List<WeatherDayStatistic>> GetWeatherInfoAsync(string city, int days);
        public WeatherDayStatistic ParseJson(string jsonResponse);
    }
}
