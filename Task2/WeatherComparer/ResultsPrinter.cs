﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherComparer
{
    public static class ResultsPrinter
    {
        public static void PrintComparison_WarmerDays(WeatherComparisonResults comparisonResults)
        {
            var resultCity1 = comparisonResults.GetResult(City.FIRST);
            var resultCity2 = comparisonResults.GetResult(City.SECOND);
            var cityName1 = comparisonResults.GetCityName(City.FIRST);
            var cityName2 = comparisonResults.GetCityName(City.SECOND);

            if (resultCity1.Count != resultCity2.Count)
            {
                throw new ArgumentException("Weather resaults count is not equal");
            }

            Console.WriteLine("Comparing...");
            var warmDaysCity1 = 0;
            var warmDaysCity2 = 0;
            for (int i = 0; i < resultCity1.Count; i++)
            {
                var resultDayCity1 = resultCity1[i];
                var resultDayCity2 = resultCity2[i];

                if (resultDayCity1.Temperature > resultDayCity2.Temperature)
                {
                    warmDaysCity1++;
                }
                else if (resultDayCity1.Temperature < resultDayCity2.Temperature)
                {
                    warmDaysCity2++;
                }
            }

            Console.WriteLine("========================================================================");
            Console.WriteLine("| Location      | {0,-23} |   | {1,-23} |", cityName1, cityName2);
            Console.Write("| WarmDays      | {0,23} | ", warmDaysCity1.ToString());
            Console.Write(warmDaysCity1 > warmDaysCity2 ? ">" : warmDaysCity1 == warmDaysCity2 ? "=" : "<");
            Console.Write(" | {0,23} |\n", warmDaysCity2.ToString());
        }

        public static void PrintComparison_RainerDays(WeatherComparisonResults comparisonResults)
        {
            var resultCity1 = comparisonResults.GetResult(City.FIRST);
            var resultCity2 = comparisonResults.GetResult(City.SECOND);
            var cityName1 = comparisonResults.GetCityName(City.FIRST);
            var cityName2 = comparisonResults.GetCityName(City.SECOND);

            if (resultCity1.Count != resultCity2.Count)
            {
                throw new ArgumentException("Weather resaults count is not equal");
            }

            Console.WriteLine("Comparing...");
            var rainDaysCity1 = 0;
            var rainDaysCity2 = 0;
            for (int i = 0; i < resultCity1.Count; i++)
            {
                var resultDayCity1 = resultCity1[i];
                var resultDayCity2 = resultCity2[i];

                if (resultDayCity1.Rain > resultDayCity2.Rain)
                {
                    rainDaysCity1++;
                }
                else if (resultDayCity1.Rain < resultDayCity2.Rain)
                {
                    rainDaysCity2++;
                }
            }

            Console.WriteLine("========================================================================");
            Console.WriteLine("| Location      | {0,-23} |   | {1,-23} |", cityName1, cityName2);
            Console.Write("| RainDays      | {0,23} | ", rainDaysCity1.ToString());
            Console.Write(rainDaysCity1 > rainDaysCity2 ? ">" : rainDaysCity1 == rainDaysCity2 ? "=" : "<");
            Console.Write(" | {0,23} |\n", rainDaysCity2.ToString());
        }

        public static void PrintComparison_Detailed(WeatherComparisonResults comparisonResults)
        {
            var resultCity1 = comparisonResults.GetResult(City.FIRST);
            var resultCity2 = comparisonResults.GetResult(City.SECOND);
            var cityName1 = comparisonResults.GetCityName(City.FIRST);
            var cityName2 = comparisonResults.GetCityName(City.SECOND);

            if (resultCity1.Count != resultCity2.Count)
            {
                throw new ArgumentException("Weather resaults count is not equal");
            }

            Console.WriteLine("Comparing...");
            for (int i = 0; i < resultCity1.Count; i++)
            {
                var resultDayCity1 = resultCity1[i];
                var resultDayCity2 = resultCity2[i];
                Console.WriteLine("========================================================================");
                Console.WriteLine("| Location      | {0,-23} |   | {1,-23} |", cityName1, cityName2);
                Console.WriteLine("| Date          | {0,53} |", resultDayCity1.Date);

                Console.Write("| Pressure      | {0,23} | ", resultDayCity1.Pressure.ToString());
                Console.Write(resultDayCity1.Pressure > resultDayCity2.Pressure ? ">" : resultDayCity1.Pressure == resultDayCity2.Pressure ? "=" : "<");
                Console.Write(" | {0,23} |\n", resultDayCity2.Pressure.ToString());

                Console.Write("| Temperature   | {0,23} | ", resultDayCity1.Temperature.ToString());
                Console.Write(resultDayCity1.Temperature > resultDayCity2.Temperature ? ">" : resultDayCity1.Temperature == resultDayCity2.Temperature ? "=" : "<");
                Console.Write(" | {0,23} |\n", resultDayCity2.Temperature.ToString());

                Console.Write("| TempFeelsLike | {0,23} | ", resultDayCity1.TemperatureFeelsLike.ToString());
                Console.Write(resultDayCity1.TemperatureFeelsLike > resultDayCity2.TemperatureFeelsLike ? ">" : resultDayCity1.TemperatureFeelsLike == resultDayCity2.TemperatureFeelsLike ? "=" : "<");
                Console.Write(" | {0,23} |\n", resultDayCity2.TemperatureFeelsLike.ToString());

                Console.Write("| TemperatureMin| {0,23} | ", resultDayCity1.TemperatureMin.ToString());
                Console.Write(resultDayCity1.TemperatureMin > resultDayCity2.TemperatureMin ? ">" : resultDayCity1.TemperatureMin == resultDayCity2.TemperatureMin ? "=" : "<");
                Console.Write(" | {0,23} |\n", resultDayCity2.TemperatureMin.ToString());

                Console.Write("| TemperatureMax| {0,23} | ", resultDayCity1.TemperatureMax.ToString());
                Console.Write(resultDayCity1.TemperatureMax > resultDayCity2.TemperatureMax ? ">" : resultDayCity1.TemperatureMax == resultDayCity2.TemperatureMax ? "=" : "<");
                Console.Write(" | {0,23} |\n", resultDayCity2.TemperatureMax.ToString());

                Console.Write("| WindSpeed     | {0,23} | ", resultDayCity1.WindSpeed.ToString());
                Console.Write(resultDayCity1.WindSpeed > resultDayCity2.WindSpeed ? ">" : resultDayCity1.WindSpeed == resultDayCity2.WindSpeed ? "=" : "<");
                Console.Write(" | {0,23} |\n", resultDayCity2.WindSpeed.ToString());

                Console.Write("| CloudsHours   | {0,23} | ", resultDayCity1.Clouds.ToString());
                Console.Write(resultDayCity1.Clouds > resultDayCity2.Clouds ? ">" : resultDayCity1.Clouds == resultDayCity2.Clouds ? "=" : "<");
                Console.Write(" | {0,23} |\n", resultDayCity2.Clouds.ToString());

                Console.Write("| RainHours     | {0,23} | ", resultDayCity1.Rain.ToString());
                Console.Write(resultDayCity1.Rain > resultDayCity2.Rain ? ">" : resultDayCity1.Rain == resultDayCity2.Rain ? "=" : "<");
                Console.Write(" | {0,23} |\n", resultDayCity2.Rain.ToString());
            }
        }
    }
}
