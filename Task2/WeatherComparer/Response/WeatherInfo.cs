﻿using System.Collections.Generic;

namespace WeatherComparer.Response
{
    public class WeatherInfo
    {
        public int dt;
        public float temp;
        public float feels_like;
        public int pressure;
        public int humidity;
        public float dew_point;
        public float uvi;
        public float clouds;
        public float visibility;
        public float wind_speed;
        public float wind_deg;
        public float wind_gust;
        public List<WeatherCondition> weather;
    }
}
