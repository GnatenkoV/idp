﻿using System.Collections.Generic;

namespace WeatherComparer.Response
{
    public class WeatherResponse
    {
        public string lat;
        public string lon;
        public string timezone;
        public float timezone_offset;
        public WeatherInfo current;
        public List<WeatherInfo> hourly;
    }
}
