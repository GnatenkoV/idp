﻿namespace WeatherComparer.Response
{
    public class WeatherCondition
    {
        public int id;
        public string main;
        public string description;
    }
}
