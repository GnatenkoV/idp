﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using WeatherComparer.Interfaces;

namespace WeatherComparer
{
    public class WeatherComparisonResults : IWeatherComparisonResults
    {
        private List<WeatherDayStatistic> m_resultCity1;
        private List<WeatherDayStatistic> m_resultCity2;
        private string m_cityName1;
        private string m_cityName2;

        public WeatherComparisonResults(List<WeatherDayStatistic> cityResult1, List<WeatherDayStatistic> cityResult2, string cityName1, string cityName2)
        {
            m_resultCity1 = cityResult1;
            m_resultCity2 = cityResult2;
            m_cityName1 = cityName1;
            m_cityName2 = cityName2;
        }

        public string GetCityName(City city)
        {
            if (city == City.FIRST)
            {
                return m_cityName1;
            }
            else
            {
                return m_cityName2;
            }
        }

        public List<WeatherDayStatistic> GetResult(City city)
        {
            if (city == City.FIRST)
            {
                return m_resultCity1;
            }
            else
            {
                return m_resultCity2;
            }
        }
    }
}
