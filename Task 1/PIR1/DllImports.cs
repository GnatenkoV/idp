﻿using System;
using System.Runtime.InteropServices;

namespace PIR1
{
    public static class Imports
    {
        [DllImport("TreeNode.dll")]
        public static extern IntPtr CreateTreeManager();

        [DllImport("TreeNode.dll")]
        public static extern bool DeleteTreeManager();

        [DllImport("TreeNode.dll")]
        public static extern IntPtr CreateRootNode();

        [DllImport("TreeNode.dll")]
        public static extern bool DeleteRootNode();

        [DllImport("TreeNode.dll")]
        public static extern IntPtr GetRootNode();

        [DllImport("TreeNode.dll")]
        public static extern int GetChildrensCount(IntPtr treeNode);

        [DllImport("TreeNode.dll")]
        public static extern bool DeleteTreeNode(IntPtr treeNode);

        [DllImport("TreeNode.dll")]
        public static extern IntPtr AddChild(IntPtr treeNode, string name);

        [DllImport("TreeNode.dll")]
        public static extern IntPtr GetName(IntPtr treeNode);

        [DllImport("TreeNode.dll")]
        public static extern int GetDepth(IntPtr treeNode);
    }
}
