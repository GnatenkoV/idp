﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: InternalsVisibleTo("TreeManager")]
namespace PIR1
{
    public class TreeNode : IDisposable
    {
        private IntPtr _handle = IntPtr.Zero;
        private string _name;
        private int _depth = 0;

        public TreeNode(IntPtr handle)
        {
            if (handle == IntPtr.Zero)
            {
                throw new NullReferenceException("LibManager handle is null");
            }
            _handle = handle;
            LoadFields();
        }

        ~TreeNode()
        {
            Dispose(false);
        }

        public string GetName()
        {
            return _name;
        }

        public int GetDepth()
        {
            return _depth;
        }

        public int GetChildsCount()
        {
            return Imports.GetChildrensCount(_handle);
        }

        private void LoadFields()
        {
            //Name 
            IntPtr p = Imports.GetName(_handle);
            if (p != IntPtr.Zero)
            {
                _name = Marshal.PtrToStringAnsi(p);
            }
            else
            {
                _name = "-";
            }

            //Depth
            _depth = Imports.GetDepth(_handle);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_handle != IntPtr.Zero && _name != "Root")
            {
                // Call the DLL Export to dispose this class
                Imports.DeleteTreeNode(_handle);
                _handle = IntPtr.Zero;
            }

            if (disposing)
            {
                GC.SuppressFinalize(this);
            }
        }

        internal IntPtr GetHandle()
        {
            return _handle;
        }
    }
}
