﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleTo("TreeManagerTest")]

namespace PIR1
{
    public class TreeManager
    {
        private TreeNode _rootNode = null;
        private List<TreeNode> _treeNodeList;

        public TreeManager()
        {
            if (Imports.CreateTreeManager() == IntPtr.Zero)
            {
                throw new NullReferenceException("LibManager handle is null");
            }
            _treeNodeList = new List<TreeNode>();
        }

        ~TreeManager()
        {
            for (int i = _treeNodeList.Count - 1; i >= 0; i--)
            {
                _treeNodeList[i].Dispose();
            }
            _treeNodeList.Clear();

            Imports.DeleteTreeManager();
        }

        public void CreateDefaultTree()
        {
            _rootNode = new TreeNode(Imports.CreateRootNode());
            _treeNodeList.Add(_rootNode);
            AddChild(0, "Node1");
            AddChild(0, "Node2");
            AddChild(0, "Node3");
            AddChild(1, "Node11");
            AddChild(1, "Node12");
            AddChild(5, "Node31");
        }

        public void DeleteTree()
        {
            _treeNodeList.RemoveAll(node => true);
        }

        public bool AddChild(int pos, string name)
        {
            if (pos < 0 || pos >= _treeNodeList.Count)
            {
                return false;
            }

            var childsCount = _treeNodeList[pos].GetChildsCount();

            var node = _treeNodeList[pos];
            var child = new TreeNode(Imports.AddChild(node.GetHandle(), name));

            _treeNodeList.Insert(pos + childsCount + 1, child);
            return true;
        }

        public string GetChildrensList()
        {
            var builder = new StringBuilder();
            
            foreach (var treeNode in _treeNodeList)
            {
                builder.AppendLine($"{new string('-', treeNode.GetDepth())}{treeNode.GetName()}");
            }

            return builder.ToString();
        }

        public int GetChildrensCount()
        {
            if (_rootNode != null)
            {
                return _rootNode.GetChildsCount();
            }
            return -1;
        }

        public bool DeleteTreeNode(int pos)
        {
            if (pos < 0 || pos >= _treeNodeList.Count)
            {
                return false;
            }
            //TODO updating
            var childsCount = _treeNodeList[pos].GetChildsCount();
            
            if (pos + childsCount < _treeNodeList.Count)
            {
                for (int i = pos + childsCount; i >= pos; i--)
                {
                    if (i == 0)
                    {
                        _rootNode.Dispose();
                        Imports.DeleteRootNode();
                        _treeNodeList.RemoveAt(i);
                        CreateDefaultTree();
                    }
                    else
                    {
                        var node = _treeNodeList[i];
                        node.Dispose();
                        _treeNodeList.RemoveAt(i);
                    }
                }
                return true;
            }
            return false;
        }

        public string GetName(int pos)
        {
            if (pos < 0 || pos >= _treeNodeList.Count)
            {
                return "-";
            }
            return _treeNodeList[pos].GetName();
        }
    };
}
