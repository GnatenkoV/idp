#pragma once

#include "lib_global.h"
#include <mutex>

#pragma warning(disable: 4251)

namespace Tree
{
    class TreeNode;

    class LIBMANAGER_DECL TreeManager
    {
    public:
        static TreeManager* get()
        {
            std::lock_guard locker(m_mutex);
            if (!m_instance)
            {
                m_instance.reset(new TreeManager());
            }

            return m_instance.get();
        }

        static void release()
        {
            if (m_instance)
            {
                m_instance.reset();
            }
        }

        TreeNode* createRootNode();
        TreeNode* getRootNode();
        bool deleteRootNode();

        bool deleteTreeNode(TreeNode* node);
        TreeNode* addChild(TreeNode* node, const char* name);
        bool contains(TreeNode* node, TreeNode* root);

    private:
        TreeManager() = default;

    private:
        static std::unique_ptr<TreeManager> m_instance;
        static std::mutex m_mutex;

        std::unique_ptr<TreeNode> m_root = nullptr;
        int m_uid = 0;
    };
}

#pragma region wrapper
EXP Tree::TreeManager* CreateTreeManager();
EXP bool DeleteTreeManager();

EXP Tree::TreeNode* CreateRootNode();
EXP bool DeleteRootNode();
EXP Tree::TreeNode* GetRootNode();

EXP Tree::TreeNode* AddChild(Tree::TreeNode* node, const char* name);
#pragma endregion wrapper