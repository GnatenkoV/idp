#include "pch.h"
#include "TreeManager.h"
#include "TreeNode.h"

namespace Tree
{
    std::unique_ptr<TreeManager> TreeManager::m_instance;
    std::mutex TreeManager::m_mutex;

    TreeNode* TreeManager::createRootNode()
    {
        if (m_root == nullptr)
        {
            m_uid = 1;
            m_root = std::make_unique<TreeNode>("Root", 0);
        }

        return m_root.get();
    }

    bool TreeManager::deleteRootNode()
    {
        if (m_root != nullptr)
        {
            m_root.release();
            return true;
        }
        return false;
    }

    bool TreeManager::deleteTreeNode(TreeNode* node)
    {
        if (node == nullptr)
        {
            return false;
        }

        if (contains(node, m_root.get()))
        {
            delete node;
            node = nullptr;
            return true;
        }
        return false;
    }

    TreeNode* TreeManager::addChild(TreeNode* node, const char* name)
    {
        return node->addChildren(name, m_uid++);
    }

    bool TreeManager::contains(TreeNode* node, TreeNode* root)
    {
        if (node == root)
        {
            return true;
        }

        for (auto* n : root->m_childrens)
        {
            if (contains(node, n))
            {
                return true;
            }
        }
        return false;
    }

    TreeNode* TreeManager::getRootNode()
    {
        return m_root.get();
    }
}
    Tree::TreeManager* CreateTreeManager()
    {
        try
        {
            return Tree::TreeManager::get();
        }
        catch (std::exception)
        {
            return nullptr;
        }
    }

    bool DeleteTreeManager()
    {
        try
        {
            Tree::TreeManager::release();
        }
        catch (std::exception)
        {
            return false;
        }
        return true;
    }

    Tree::TreeNode* CreateRootNode()
    {
        try
        {
            return Tree::TreeManager::get()->createRootNode();
        }
        catch (std::exception)
        {
            return nullptr;
        }
    }

    bool DeleteRootNode()
    {
        try
        {
            return Tree::TreeManager::get()->deleteRootNode();
        }
        catch (std::exception)
        {
            return false;
        }
    }

    Tree::TreeNode* GetRootNode()
    {
        try
        {
            return Tree::TreeManager::get()->getRootNode();
        }
        catch (std::exception)
        {
            return nullptr;
        }
    }

    Tree::TreeNode* AddChild(Tree::TreeNode* node, const char* name)
    {
        try
        {
            return Tree::TreeManager::get()->addChild(node, name);
        }
        catch (std::exception)
        {
            return nullptr;
        }
    }


