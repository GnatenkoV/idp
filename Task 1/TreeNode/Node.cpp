#include "pch.h"
#include "Node.h"

Node::~Node()
{
    removeAll();
}

Node::Node(const char* name, Node* parent) : m_name(name), m_parent(parent)
{
}

void Node::init(std::vector<std::string> &cfg, int depth)
{
    if (m_depth != -1) return;
    m_depth = depth;
    int d = 1;
    while (!cfg.empty()) {
        std::string token = cfg.front();
        for (auto& c : token){
            if (c == '-') {
                d++;
            }
        }
        token.erase(token.begin(), token.begin() + d - 1);
        if (d == m_depth){
            cfg.erase(cfg.begin(), cfg.begin() + 1);
            auto child = new Node(token.c_str(), this);
            child->init(cfg, d + 1);
            m_childrens.push_back(child);
        }else{
            return;
        }
        d = 1;
    }
}

const char* Node::getName()
{
    return m_name.c_str();
}

int Node::childrenCount()
{
    return static_cast<int>(m_childrens.size());
}

Node* Node::getChildren(int pos)
{
    if (pos < 0 || pos >= m_childrens.size()) return nullptr;
    return m_childrens[pos];
}

void Node::removeAll()
{
    for (int i = childrenCount() - 1; i >= 0; i--){
        delete m_childrens[i];
    }
    m_childrens.clear();
}

void Node::getChildrens(std::string& list)
{
    for (int i = 0; i < m_depth-1; i++)
    {
        list.push_back('-');
    }
    
    list.append(m_name + '\n');

    for (int i = 0; i < childrenCount(); i++)
    {
       m_childrens[i]->getChildrens(list);
    }
}

Node* Node::addChildren()
{
    auto ch = new Node(std::to_string(childrenCount()+1).c_str(), this);
    ch->m_depth = m_depth + 1;
    m_childrens.push_back(ch);
    return ch;
}

void Node::removeChildren(int pos)
{
    if (childrenCount() == 0) return;
    if (pos >= 0 && pos < childrenCount())
    {
        auto ch = m_childrens[pos];
        m_childrens.erase(m_childrens.begin() + pos);
        delete ch;
    }
}

void Node::removeChildren(Node* node)
{
    int pos = 0;
    for (const auto* child : m_childrens)
    {
        if (child == node)
        {
            removeChildren(pos);
            break;
        }
        pos++;
    }
}

void Node::deleteItself()
{
    if (m_parent != nullptr) 
    {
        m_parent->removeChildren(this);
    }
}

Node* Node::find(int pos)
{
    return findRec(pos);
}

const char* Node::saveChildList(const std::string& data)
{
    releaseChildList();
    if (data.empty()) return "";
    size_t len = data.length();
    m_childList = new char[len + 1];
    strcpy_s(m_childList, len + 1, data.c_str());
    m_childList[len] = '\0';
    return m_childList;
}

void Node::releaseChildList()
{
    if (m_childList != nullptr)
    {
        delete[] m_childList;
        m_childList = nullptr;
    }
}

Node* Node::findRec(int &pos)
{
    if (pos < 0) return nullptr;
    if (pos == 0) return this;
    for (int i = 0; i < childrenCount(); i++)
    {
        auto node = getChildren(i)->findRec(--pos);
        if (node != nullptr) return node;
    }
    return nullptr;
}

int GetChildrensCount(Node* node)
{
    if (node != nullptr) {
        return node->childrenCount();
    }
    return -1;
}

void RemoveNode(Node* node)
{
    if (node != nullptr)
        node->deleteItself();
}

void AddChild(Node* node)
{
    if (node != nullptr) {
        node->addChildren();
    }
}

const char* GetName(Node* node)
{
    if (node != nullptr) {
        return node->getName();
    }
    return "";
}

const char* GetChildrensList(Node* node)
{
    if (node != nullptr) {
        std::string data;
        node->getChildrens(data);
        return node->saveChildList(data);
    }
    return "";
}

void ReleaseChildrenList(Node* node)
{
    if (node != nullptr) {
        node->releaseChildList();
    }
}

Node* GetChildren(Node* node, int pos)
{
    if (node != nullptr) {
        return node->getChildren(pos);
    }
    return nullptr;
}